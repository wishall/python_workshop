# # ############################
# # day1
# # ############################

# variable and assignment

# # variable
# # valid
# # 1) variable starting character
# # 2) variable starting underscore
# # 3) variable starting char followed by the number
# # ############################
# # valid eg
# a=24
# print(a)
# # #############################
# _a=23
# print(_a)
# a1=44
# print(a1)
# # ##################
# # invalid
# @a=23
# #print(@a)
# $a=23
# print($a)
# # ######################
# # python support multipal assignment
# a,b=23,45
# print(a,b)
# ######################
# for swapping of two numbers
# #####################
# # number system
# a=23
# b=23.34
# c=24+45j
# print(a,b,c)
# print(type(a),type(b),type(c))
# #
# #string
# a='hello all'
# b="good evening"
# c='''enjoy your tea'''
# d="""be ready for RBG system"""
# print(a,b,c,d,type(a))
# ## list
# list1=[1,2,3,4,5]
# list2=[2,"hello",[2,3,4]]
# print(list1)
# print(list2)
# print(type(list1))
# #touple
# tup1=(1,2,3,4,5)
# tup2=(2,"hello",(2,3,4))
# print(tup1)
# print(tup2)
# print(type(tup1))
# #####################
# ##dictionary
# dict1={"name":"vishal",
#       "std":"SE",
#       "roll_no":21,
#       "courses":["java","python"]}
# print(dict1)
# print(type(dict1))
# ########################3
# ##set
# set1={1,2,3,4,5}
# set2={2,3,4,5,6}
# print(set1.issubset(set2))
# print(set1)
# print(set2)
# print(type(set2))
# ##########################
# # Arithmetic
# ###################
# a=30
# b=5
# print(f"add :-{a+b},sub :-{a-b}")
# print(f"multipal :-{a*b}")
# ##note
# # f""-->
# a=3
# b=2
# print(a/b)#-->gives an exact division value
# print(a//b)#==>givesQuotient value
# a=-3#if any one of the value is nagative then we got the value which is lowre.i.e floor value
# print(a/b)
# print(a//b)
# a=10
# b=3
# print (a**b)
# ###################
# a=3
# b=10
# a+=b
# print(a)
# #####################
# #comparison
# a=10
# b=12
# c=14
# print(a>b)
# print(a<=b)
# print(a>=b)
# print(a==c)
# print(a<b>c)
# ####################3
# ##logical
# a=0
# b=23
# c=25
# print(a and b and c)
# print(a or b or c)
# print(a or b and b or c)
# print(a-b or b-a or a-c or c-a)
# ## bitwise
# a=5
# b=10
# print(a&b)#0101*1010=0000
# print(a|b)#0000+1111=1111
# print(b>>1)#1010>0101=0101
# #######################################
# #membership
# list1=[1,2,3,4,5]
# # a="hello world"
# # print(2 in list1)
# # print('w' in a)
# print('world' not in a)
# a=23
# b="23"
# print(type(a) is type(b))
# # print(type(a) is not type(int(b)))
# a=input("Enter no")
# print(a)
# print(type(a))
####################################
# # ### code to root###
# # print(float(input("Enter no!"))**(1/float(input("Enter the root"))))
# ################################
# ## decision amking
# ##############################3
# # ## if statement
# a=23
# b=24
# # if a>b:
# #     print("a is greater!!")
# # else:
# # 	print("b is greater!!")
# print("a is greater") if (a>b) else print("b is greater")
#####################
##if elif else statement
# a=10
# b=15
# c=20
# if a>b and a>c:
# 	print("a is greater")
# elif b>a and b>c:
# 	print("b is greater")
# else:
#  print("c is greater")
#  ##################
# ##if sttment
# # print("a is greater") if(a>b and a>c) else print("b is greater") if(b>c and b>a) else print("c is greater")
# ######################################
# #Note:python does not support switch case
# #range([start],stop,[jump])
# print(range(1,10))
# print(list(range(1,10)))
# print(list(range(20)))
# print(list(range(0,100,2)))
# print(list(range(99,1,-2)))
# ######################################
# # loop
# # for
# for i in "hello world":
# 	print(i)
# for i in "hello world":
# 	print(i,end=" ") 
# ##############################
# for i in list(range(1,23)):
# 	for j in list(range(1,11)):
# 		print(i*j,end=" ")
# 	print()
# #############################
# counter=0
# while counter<=10:
# 	print("hello all")
# else:
# 	print("good by")
# ##################################
# #control statements
