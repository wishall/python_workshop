from rest_framework import serializers
from .models import Admin,Course,Student

class AdminSerializer(serializers.ModelSerializer):
	class Meta:
		fields="__all__"
		model=Admin

class CourseSerializer(serializers.ModelSerializer):
	class Meta:
		fields="__all__"
		model=Course

class StudentSerializer(serializers.ModelSerializer):
	class Meta:
		fields="__all__"
		model=Student