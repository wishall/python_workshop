from django.db import models
import uuid

# Create your models here
def upload_admin_avator(instance,filename):
	return f"admin/avator/{instance.admin_id}/{instance.admin_name}/{filename}"
def upload_student_avator(instance,filename):
	return f"student/avator/{instance.student_id}/{instance.student_name}/{filename}"
class Admin(models.Model):
	admin_id=models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	admin_name=models.CharField(max_length=30)
	admin_contact_no=models.BigIntegerField()
	admin_email_id=models.EmailField(unique=True)
	admin_avator=models.ImageField(upload_to=upload_admin_avator)
	admin_address=models.TextField()
	admin_created_at=models.DateTimeField(auto_now_add=True)
	admin_updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.admin_name
class Course(models.Model):
	location=(("KD","Kandivali"),("DD","DADAR"),("CH","CHEMBUR"))
	course_id=models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	course_name=models.CharField(max_length=30,unique=True)
	course_start_date=models.DateField()
	course_end_date=models.DateField()
	course_fees=models.FloatField()
	course_location=models.TextField(max_length=20,choices=location)
	admin=models.ForeignKey(Admin,on_delete=models.CASCADE)
	course_created_at=models.DateTimeField(auto_now_add=True)
	course_updated_at=models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.course_name
class Student(models.Model):
	student_id=models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	student_name=models.CharField(max_length=30)
	student_contact_no=models.BigIntegerField()
	student_email_id=models.EmailField(unique=True)
	student_avator=models.ImageField(upload_to=upload_student_avator)
	student_address=models.TextField()
	student_created_at=models.DateTimeField(auto_now_add=True)
	student_updated_at=models.DateTimeField(auto_now=True)
	courses=models.ManyToManyField(Course)
	admin=models.ForeignKey(Admin,on_delete=models.CASCADE)
	def __str__(self):
		return self.student_name

