from .views import AdminViewSet,CourseViewSet,StudentViewSet
from rest_framework.routers import DefaultRouter

router=DefaultRouter()
router.register('admin',AdminViewSet,base_name="admin")
router.register('course',CourseViewSet,base_name="course")
router.register('student',StudentViewSet,base_name="student")
urlpatterns=router.urls