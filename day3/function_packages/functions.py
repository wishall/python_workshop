# function programing
#####################
# # Declaration
# def printsomething():
# 	print("HELLO ALL❤❤")
# 	# calling statement
# a=printsomething
# a()

###############
# TYPE 2 FUNCTION WITHOUT PARAMETER
# a=10 #global varieble
# b=20 #globle variable
# def addit():
# 	print(a+b)
# addit() #calling addnow
# #//
# def addit():
# 	def addnow():
# 		print(a+b) # a and b are globle
# 	addnow() # calling addnow
#############
# addit() # calling addit
###################

# TYPE 3 FUNCTION WITH PARAMETER
##
# Declaration 
def calculator(a,b):
	return(a+b,a-b,a*b,a/b,a//b,a*b,a**b)
#####
# # calling function
#add,sub,mul,div,fdiv,mul,power=calculator(5,5)
#print(f"addition-->{add},substraction-->{sub}")
#print(add,sub,mul,div,fdiv,mul,power)
#######################

# TYPE 4 FUNCTION WITH DEFAULT VALUE
#declaration
# def divideit(a=1,b=1):
#     print(a/b)
# divideit()
# divideit(5)
# divideit(5,10)
# divideit(b=10,a=20)
# divideit(10,)
# divideit(b=10)
# divideit(,10) #syntax error

###############

# TYPE 5 -->FUNCTION WITH ARGS
##
# #dECLERATION
# def iamwithArgs(*a):
# 	for i in list(a):
# 		if(type(i) is type(1234)):
# 			print(i)	        
# ######
# # calling
# iamwithArgs(1,2,3,"hello",[1,2,3,34,"hii"])
####################
# def elementlist(*a):
# 	for i in list(a)
# 	    if(type(i) is type([])):
# 	    	for j in i:
# 	    		print(j)
# ##############
# # calling
# elementlist(1,2,3,"hello",[1,2,3,34,"hii"])

#######################

# TYPE 6 -->FUNCTION WITH KWARGES
##
# dECLARATION
def iamwithkwarges(**kwarges):
	print(kwarges)
##########
# # calling of kwarges
# iamwithkwarges(name="vishal",std="SE")

#####################################

# TYPE 7--> FUNCTION WITH ARGS AND KWARGES
# Declaration

def iamwaitingArgandKwarges(*args,**kwarges):
	print(args)
	print(kwarges)
# ##calling 
# iamwaitingArgandKwarges(1,2,3,"hello",name="vishal")

######################################

#TYPE 8-->LAMBDA FUNCTION
##
# inc=lambda x:x+1
# dec=lambda x:x-1
# print(inc(35))
# print(dec(35))
# #//
# great=lambda x,y:x if(x>y)else y
# print(great(10,20))

#########################

# TYPE 9-->MAP FUNCTION
## MAP(FUNCTION,DATA)//map contain function and data
#  map is use of applying bussiness logic in function
# data=list(range(1,7)) #function
# business_list=(lambda x:x*x) # data
# print(list(map(business_list,data)))
##
# data=["GOA","maHarashtra","PuNJAB","LuckNow","kashmir"]
# print(data)
# def checkcase(x):
# 	if(x.islower()):
# 		return(x.upper())
# 	elif(x.isupper()):
# 		return(x.lower())
# 	else:
# 		return(x.swapcase())
# business_logic=lambda x:checkcase(x)
# print(list(map(business_logic,data))

###############################

# TYPE 10-->FILTER FUNCTION
# FILTER(LOGIC,DATA)
# data=list(range(1,11))
# logic=lambda x:x%2==0
# print(list(filter(logic,data)))
###############################

# # TYPE 11--> FUNCTION REDUCE
# ##
# data=list(range(1,10))
# from functools import reduce # importing package is required
# print(reduce(lambda x,y:x+y,data))

#################

# # TYPE 12--> ZIP FUNCTION
# ##
# list1=("red","black","orange")
# list2=(1,2,3,4)
# compress_data=zip(list1,list2)
# print(tuple(compress_data))

#################

# TYPE 13-->ENUMERATE FUNCTION
##
# list1=["red","green","orange"]
# for index,data in enumerate(list1):
# 	print(index,data)

# ####################

# TYPE 14-->CLOUSER ,DECORATOR OR FIRST CLASS CITIZEN FUNCTION
# ##CLOUSER
# def wrapper(funcn):
# 	def inner():
# 		print("before execution")
# 		funcn()
# 		print("after execution")
# 	return inner
# def printme():
# 	print("hello all")
# printme=wrapper(printme)
# printme()

##DECORATOR
# def wrapper(funcn):
# 	def inner():
# 		print("before execution")
# 		funcn()
# 		print("after execution")
# 	return inner
# @wrapper #printme=wrapper(printme)
# def printme():
#     print("hello all")
# printme()
# ###
# def checkvalue(func):
# 	def inner(*args,**kw):
# 		if(type(args[0])==type("str") or type(args[1])==type("str") ):
# 			print("Check u r parameter")
# 		else:
# 			func(*args,**kw)
# 	return inner
# @checkvalue
# def add(a,b):
# 	print(a+b)
# add(3,4)

#########################################################

############### EXCEPTION HANDLING  ###############
##########
def firstflow():
	try:
		no1=input("Enter no1")
		no2=input("Enter no2")
		div=no1/no2
	except Exception as e:
		print("ERROR OCCURED,TRASFERRING ALTERNATE FIRSTFLOW")
		alternateflow(no1,no2)
	else:
		print(f"division value is{div}")
def alternateflow(no1,no2):
	try:
		if type(no1)==type("") or type(no2)==type(""):
			no1=float(no1)
			no2=float(no2)
			div=(no1/no2)
		else:
			div=no1/no2
	except Exception as e:
		print("Error occoured in alternate flow")
		firstflow()
	else:
		print(f"division value is {div}")
#firstflow()

def customException():
	try:
		no1=float(input("Enter no1"))
		no2=float(input("Enter no2"))
		if((no1/no2)%2==0):
			output=(f"division is {no1/no2}and it is even!!")
		else:
			raise BaseException("Division output is not even")
	except BaseException as e:
		if (input("do you want to see error message(y/n)")=="y"):
			print(e)
		print("try again!!")
		customException()
	else:
		print(output)
#customException()

def insertData(username,password):
	try:
		import pymysql as sql
		db=sql.connect("localhost","root","","python_demo")
		cursor=db.cursor()
		sql=f"insert into user(username,password)values('{username}',{password})"
		cursor.execute(sql)
		db.commit()
		if(cursor.rowcount>0):
		    output="data inserted successfully"
		else:
			output="data not insreted"
	except Exception as e:
		db.rollback()
		print(e)  
	else:
	    print(output) 
username=input("Enter user name!!")
password=input("Enter password!!")
# insertData(username,password)

def getallData(): 	
	try:
		import pymysql as sql
		db=sql.connect("localhost","root","","python_demo")
		cursor=db.cursor()
		sql="select * from user"
		cursor.execute(sql)
		rs=cursor.fetchall()
	except Exception as e:
		raise e
	else:
		print(rs)
getallData()









