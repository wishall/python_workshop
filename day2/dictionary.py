# dict1={
# 	"name":"VISHAL",
# 	"std":"SE",
# 	"roll no":21,
# 	"courses":["java","python"],
# 	"address":{"landmark":"mulund","pincode":"400033"},
# 	}
# print(dict1)
# ### read
# printg(dict["std"])

# ##update
# dict1["name"]="raj"
# print(dict1)
# note:->update note disc is mutable therefore update n del is possible
# dic1["name"="raj"]
# print(dict1)

# ###del
# del dic["name"]
# print(dict1)


######################
##Dictionary methods
# clear()
# copy()
# fromkeys()
# __contains__()
# #has_keys

# get()
# setdefault()

# #update
# keys()
# values()
# items()
# dict1.clear()
# print(dict1)

dict1={"name":"vishal",
    "std":"SE",
    "roll_no":21}
dict2=dict1
dict3=dict1.copy()
print(dict1)
print(dict2)
print(dict3)
dict1["name"]="wishall"
print(dict1)
print(dict2)
print(dict3)
## usage of copy function:->only used whane our data type is mutable data type
###########################
list1=["name","std","roll_no","address"]
print({}.fromkeys(list1,"NA"))
#######################
print(dict1.__contains__("name"))
#######################
print(dict1.get("name"))
#######################
print(dict1.setdefault("address"))
print(dict1)
########################
dict2={"name":"sid","contact_no":"8356051171",
    "address":"mulund"}
dict1.update(dict2)
print(dict1)
####################
print(list(dict1.keys()))
print(list(dict1.values()))
print(list(dict1.items()))
print(list(dict1.keys()))
print(list(dict1.values()))
print(list(dict1.items()))
