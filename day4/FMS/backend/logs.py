class Log():
	def __init__(self,filename="log.html",path=None,title=None,msg=None):
		try:
			if(not path):
				file=open(filename,"a+")
				file.write(f"<div><div><h1>{title}</h1><div><h3>{msg}</div>")
			else:
				file=open(path+filename,"a+")
				file.write(f"<div><div><h1>{title}</h1><div><h3>{msg}</div>")
				file.close()
		except BaseException as e:
			print(f"error while logging!!-->{e}")
		else:
			print(f"Log Done-->{title}")
# Log(title="Testing",msg="hello all !!")
# Log(path="C:/Users/goraksha/Desktop/",title="texting with path",msg="hello all")
