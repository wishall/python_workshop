from django.shortcuts import render

# Create your views here.

from rest_framework import viewsets
from .models import Admin,Course,Student
from .serializer import AdminSerializer,CourseSerializer,StudentSerializer
from rest_framework.permissions import IsAuthenticated

class AdminViewSet(viewsets.ModelViewSet):
	serializer_class=AdminSerializer
	queryset= Admin.objects.all()
	permission_classes=(IsAuthenticated,)

class CourseViewSet(viewsets.ModelViewSet):
	serializer_class=CourseSerializer
	queryset= Course.objects.all()
	permission_classes=(IsAuthenticated,)

class StudentViewSet(viewsets.ModelViewSet):
	serializer_class=StudentSerializer
	queryset= Student.objects.all()
	permission_classes=(IsAuthenticated,)