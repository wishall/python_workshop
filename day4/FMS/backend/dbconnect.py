from logs import Log
class DB():
	def __init__(self,ip="localhost",username="root",pwd="",dbname="FMS"):
		try:
			self.__db,self.__cursor=self.connectToMySql(ip,username,pwd,dbname)
			Log(filename="xyz.html",path="C:/Users/goraksha/Desktop/",title="db and cursor object",msg="initionalized from mysql")
		except BaseException as e :
			self.__db,self.__cursor=self.connectWithSQLite(dbname)
			Log(title="db and cursor object",msg="initionalized from sqlite")
	def connectWithSQLite(self,dbname):
		try:
			import sqlite3 as sql
			db=sql.connect(dbname)
			cursor=db.cursor()
		except BaseException as e:
			print(e)
			Log(title="DB not connected with sqlite",msg=f"error-->{e}")
		else:
			print("connected with sqlite")
			Log(title="DB connect",msg="connected with sqlite")			
			return db,cursor
	def connectToMySql(self,ip,uname,pwd,dbname):
		try:
			import pymysql as sql
			db=sql.connect(ip,uname,pwd,dbname)
			cursor=db.cursor()
		except BaseException as e:
			print(e)
			Log(title="DB not connected with mysql",msg=f"error-->{e}")
		else:
			print("connected with mysql")
			Log(title="DB connect",msg="connected with mysql")			
			return db,cursor
	def getDBCursorObject(self):
		Log(title="DB object and cursor",msg="connected with sqlite")
		return(self.__db,self.__cursor)
DB()



