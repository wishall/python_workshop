# oops
# Basic class decleration
# class Teacher():
# 	pass
# # object creation
# teacher=Teacher()
# teacher.name=input("Enter the teacher name:")
# teacher.subject=input("Enter the subject name:")
# print(teacher.name,teacher.subject)
# print(teacher.__dict__)
# print(Teacher.__dict__)
###########################
## CONSTRUCTOR
# ####
# class Teacher():
# 	# self--> referance of object
# 	#__int__ --> constructor
# 	def __init__(self):
# 		print("Hello iam in constructor")
# t1=Teacher()
# t2=Teacher()
##########################
# parametarized constructer
# ##
# class Teacher():
# 	def __init__(self,name,subject,nickname,contact_no):
# 		print(name,subject,nickname,contact_no)
# t1=Teacher("raj","java","prince","12")
######################
# class Employee():
#  	def __init__(self,name,salery):
#  		self.name=name
#  		self.salery=salery
#  	def getName(self):
#  		return self.name
#  	def getSalery(self):
#  		return self.salery
#  	def __str__(self):
#  		return self.name
#  	def getSaleryWithTax(self,tax=0.15):
#  		return self.salery+(self.salery*tax)
#  	def __add__(self,obj):
#  		return self.salery+obj.salery
# e1=Employee("wish",50000)
# e2=Employee("vish",60000.30)
# print(e1.getName())
# print(e1.getSalery())
# print(e1.getSaleryWithTax())
# print(e1.getSaleryWithTax(tax=0.17))
# print(e1,e2)
##############################
## INHERITENCE ##
# child
# class GrandParent():
# 	def getMoney(self):
# 		print("TAKING MONEY FROM GRAND_PARENT 3:")
# class Parent2(GrandParent):
# 	def getMoney(self):
# 		print("TAKING MONEY FROM PARENT 2:")
# class Parent1(Parent2):
# 	def getMoney(self):
# 		print("TAKING MONEY FROM PARENT 1:")
# class Child(Parent1):
# 	def getMoney(self):
# 		print("I LOVE @ME..😜!!")
# c1=Child()
# c1.getMoney()
# ##################################
# ##MULTIPAL INHERITANCE
# class GrandParent():
# 	def getMoney(self):
# 		print("TAKING MONEY FROM GRAND_PARENT 3:")
# class Parent2(GrandParent):
# 	# def getMoney(self):
# 	# 	print("TAKING MONEY FROM PARENT 2:")
# 	pass
# class Parent1(Parent2):
# 	# def getMoney(self):
# 	# 	print("TAKING MONEY FROM PARENT 1:")
# 	pass
# class Child(Parent1):
# 	# def getMoney(self):
# 		# print("I LOVE @ME..😜!!")
# 		pass
# c1=Child()
# c1.getMoney()
#################################
# DATA HIDING
# class Employee():
# 	def __init__(self,name,salery):
# 		self.name=name
# 		self.salery=salery
# 	def getSalery(self):
# 		return self.__getSaleryWithTax()
# e1=Employee("wish",50000)
# print(e1.getSalery())
# # print(e1.salery)
# e1.salery+=100000
# print(e1.getSalery())
